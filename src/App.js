import React, { Component } from 'react';
import './App.css';
import { BrowserRouter as Router,
    Route,
    Switch
} from 'react-router-dom';
import { createStore } from 'redux'
import { Provider } from 'react-redux';
import reducer from './reducers/reducers'
import Game from './game/GameContainer'
import Scores from './scores/ScoreContainer'

const store = createStore(reducer)

class App extends Component {
    render() {

        return (
            <Provider store={store}>
                    <Router>
                        <Switch>
                            <Route
                                exact path="/"
                                render={props => (<Game {...props}/>)}
                            />
                            <Route
                                exact path="/scores"
                                render={props => (<Scores {...props}/>)}
                            />
                        </Switch>
                    </Router>
            </Provider>
        )
    }
}

export default App