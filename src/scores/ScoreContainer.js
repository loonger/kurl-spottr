/**
 * Created by Ivan de la Fuente on 23/11/2017.
 */
import React, { Component } from 'react'
import { connect } from 'react-redux'
import {addScore, stop} from "../actions/actions"
import Scores from "./Scores"

const mapStateToProps = function(state) {
    return {
        points: state.points,
        win: state.win,
        scores: state.scores
    }
}

class ScoreContainer extends Component{

    onEnterName =(name)=>{
        const {dispatch,points} = this.props
        dispatch(addScore({name,points}))
        dispatch(stop())
    }

    render() {

        const {win,points,scores} = this.props

        return (
            <Scores
                onEnterName={this.onEnterName}
                scores={scores}
                points={points}
                win={win}
            />
        )
    }
}

export default connect(mapStateToProps)(ScoreContainer)
