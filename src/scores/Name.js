import React from 'react'

class Name extends React.Component{

    state={
        name:''
    }

    onChange =(event)=>{
        this.setState({name:event.target.value})
    }

    render(){
        const {onEnterName} = this.props
        const {name} = this.state

        return(
            <div style={styles.container}>
                <input
                    type='text'
                    className='name'
                    placeholder='Enter your name...'
                    value={name}
                    onChange={this.onChange}
                />
                <button onClick={()=>onEnterName(name)}>OK</button>
            </div>
        )
    }

}

const styles = {
    container:{
        display:'flex',
        marginLeft:'auto',
        height: 40,
        marginTop: 20
    }
}

export default Name