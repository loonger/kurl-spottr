import React, { Component } from 'react'

class ScoresTable extends Component{

    render() {

        const {scores} = this.props
        let orderedScores = scores.sort((a,b)=> b.points-a.points).filter((s,i)=>i<10)

        return(
            <div style={styles.container}>
                <h2>SCORES TABLE</h2>
                {orderedScores.map((score,i)=>
                    <div key={i} style={styles.row}>
                        <h3 style={{flex:0.1}}>{i+1}</h3>
                        <h3 style={{flex:0.7}}>{score.name}</h3>
                        <h3 style={{flex:0.2}}>{score.points}</h3>
                    </div>
                )}
            </div>
        )
    }
}


const styles = {
    container:{
        display:'flex',
        padding:15,
        flex:1,
        flexDirection:'column'
    },
    row:{
        display:'flex',
        flex:1
    }
}

export default (ScoresTable)