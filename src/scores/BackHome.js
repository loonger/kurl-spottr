import React from 'react'
import {Link} from "react-router-dom"

const BackHome =()=> {

    return(
    <div style={styles.container}>
        <Link to={'/'}>
            <h3 className='link'>HOME</h3>
        </Link>
    </div>
    )
}

const styles = {
    container:{
        display:'flex',
        marginLeft:'auto'
    }
}

export default BackHome