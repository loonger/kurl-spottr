import React from 'react'
import Name from "./Name"
import ScoresTable from "./ScoresTable"
import BackHome from "./BackHome"

const Scores =({win,points,scores,onEnterName})=> {
    return(
    <div className='container'>
        <div className='content'>
            <div className='header'>
                <h1>Kurl Spotter</h1>
                {win &&
                    <Name points={points} onEnterName={onEnterName}/>
                }
                {!win &&
                    <BackHome/>
                }
            </div>
            {win &&
                <h3 style={{marginLeft:20}}>{'YOU '+(win === 'yes' ? 'WON' : 'LOST')+' THE GAME WITH: '+points+' POINTS'}</h3>
            }
            <ScoresTable scores={scores}/>
        </div>
    </div>
    )
}

export default Scores