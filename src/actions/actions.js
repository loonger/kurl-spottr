export const start = () => ({
    type: 'START'
})

export const stop = () => ({
    type: 'STOP'
})

export const levelUp = () => ({
    type: 'LEVEL_UP'
})

export const addPoints = (points) => ({
    type: 'ADD_POINTS',
    payload: points
})

export const win = (state) => ({
    type: 'WIN_GAME',
    payload: state
})

export const addScore = (score) => ({
    type: 'ADD_SCORE',
    payload: score
})

