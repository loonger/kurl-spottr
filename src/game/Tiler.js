import React, { Component } from 'react'
import {levelUp} from "../actions/actions"
import {connect} from "react-redux"

class Tiler extends Component{

// .sort((a, b)=> b-a)

    state={}

    getRandomInt =(min, max)=>
        Math.floor(Math.random() * (max - min + 1)) + min

    getColors = ()=> {
        const {level} = this.props

        let hue = this.getRandomInt(0,360)
        let luminosity = 50+Math.round((30-level))
        let color = 'hsl('+hue+',100%,50%)'
        let winColor = 'hsl('+hue+',100%,'+luminosity+'%)'

        let nOfTiles = Math.pow(level+1,2);
        let winTile = this.getRandomInt(1,nOfTiles)

        this.setState({color,winColor,winTile})
    }

    onTileClick = (key)=> {
        const {winTile} = this.state
        if(key===winTile)this.props.dispatch(levelUp())
    }

    componentWillMount(){
        this.getColors();
    }

    componentWillReceiveProps(nextProps){
        console.log(nextProps)
        if(nextProps.level!==this.props.level){
            this.getColors();
        }
    }

    render() {

        const {level} = this.props
        const {color,winColor,winTile} = this.state
        let key=0

        return(
            <div style={styles.container}>
                {Array.apply(null, Array(level+1)).map((a,b) =>{
                    return(
                        <div style={styles.row} key={'row'+b}>
                        {Array.apply(null, Array(level+1)).map(() =>{
                            key++
                            return(
                                <Tile
                                    flex={100/(level+1)}
                                    color={key === winTile ? winColor : color}
                                    key={key}
                                    id={key}
                                    onTileClick={this.onTileClick}
                                />)
                        })}
                    </div>)
                })}
            </div>
        )
    }
}

const Tile =({flex,color,onTileClick,id})=>{
    return(
        <div
            style={{
                width:flex+'%',
                paddingBottom:flex+'%',
                backgroundColor:color,
                margin:5,
                cursor:'pointer'
            }}
            onClick={()=>onTileClick(id)}
        >
        </div>
    )
}

const styles = {
    container:{
        display:'flex',
        padding:15,
        flex:1,
        flexDirection:'column'
    },
    row:{
        display:'flex',
        flex:1
    }
}

export default connect()(Tiler)