import React from 'react'
import Scoring from "./Scoring"
import Starter from "./Starter"
import Tiler from "./Tiler"

const Game =({level,playing,onStart,points,time})=> {
    return(
    <div className='container'>
        <div className='content'>
            <div className='header'>
                <h1>Kurl Spotter</h1>
                {playing &&
                    <Scoring points={points} time={time}/>
                }
                {!playing &&
                    <Starter onStart={onStart}/>
                }
            </div>
            {playing &&
                <div className='game'>
                    <Tiler level={level}/>
                </div>
            }
        </div>
    </div>
    )
}

export default Game