/**
 * Created by Ivan de la Fuente on 26/16/2018.
 */
import React, { Component } from 'react'
import { connect } from 'react-redux'
import Game from './Game'
import {addPoints, start, win} from "../actions/actions"
import {Redirect} from "react-router-dom"

const mapStateToProps = function(state) {
    return {
        level: state.level,
        points: state.points,
        playing: state.playing
    }
}

class GameContainer extends Component{

    state = {
        time:10
    }

    lostGame = ()=> {
        this.props.dispatch(win('no'))
    }

    winGame = ()=> {
        this.props.dispatch(win('yes'))
    }

    componentWillReceiveProps(nextProps){
        const {level} = this.props
        const {time} = this.state

        if(nextProps.playing && nextProps.level===1){
            this.int = setInterval(()=>{
                if(this.state.time===1){
                    this.lostGame()
                    this.setState({finished:true})
                }
                this.setState({time:this.state.time-1})
            },1000)
        }
        if(nextProps.level !== level){
            this.setState({time:10})
            const points = time * 10 + level * 5

            this.props.dispatch(addPoints(points))
        }
        if(nextProps.level === 21){
            this.winGame()
            this.setState({finished:true})
        }
    }

    componentWillUnmount(){
        clearInterval(this.int)
    }

    render() {

        const {level,playing,points} = this.props
        const {time,finished} = this.state

        if (finished){
            return(
                <Redirect to="/scores" push />
            )
        }

        return (
            <Game
                points={points}
                playing={playing}
                level={level}
                time={time}
                onStart={()=>this.props.dispatch(start())}
            />
        )
    }
}

export default connect(mapStateToProps)(GameContainer)