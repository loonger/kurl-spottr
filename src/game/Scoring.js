import React from 'react'

const Scoring =({points,time})=> {

    return(
        <div style={styles.container}>
            <h3 className='link'>TIME: <span>{time}</span></h3>
            <h3 className='link'>SCORE: <span>{points}</span></h3>
        </div>
    )
}

const styles = {
    container:{
        display:'flex',
        marginLeft:'auto'
    }
}
export default Scoring