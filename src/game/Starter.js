import React from 'react'
import {Link} from "react-router-dom"

const Starter =({onStart})=> {

    return(
    <div style={styles.container}>
        <h3 className='link' onClick={onStart}>START</h3>
        <Link to='/scores'>
            <h3 className='link'>SCORES</h3>
        </Link>
    </div>
    )
}

const styles = {
    container:{
        display:'flex',
        marginLeft:'auto'
    }
}

export default Starter