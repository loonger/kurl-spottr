const reducers = (state = {level:1,playing:false,points:0,scores:[],win:null}, action) => {
    console.log(action)
    switch (action.type) {
        case 'LEVEL_UP':
            return {
                ...state,
                level: state.level+1
            }
        case 'ADD_POINTS':
            return {
                ...state,
                points: state.points + action.payload
            }
        case 'START':
            return {
                ...state,
                playing:true
            }
        case 'STOP':
            return {
                ...state,
                playing:false,
                points:0,
                level:1,
                win:null
            }
        case 'ADD_SCORE':
            return {
                ...state,
                scores:[...state.scores,action.payload]
            }
        case 'WIN_GAME':
            return {
                ...state,
                win: action.payload
            }
        default:
            return state
    }
}

export default reducers