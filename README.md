# Kurl Spotter - Adidas code testing

This project was bootstrapped with [Create React App](https://github.com/facebookincubator/create-react-app).

## Installation

From your system console, first clone the repository:

```
$ git clone https://loonger@bitbucket.org/loonger/kurl-spottr.git
```

Enter to the project folder:

```$ cd kurl-spottr```

Install the dependencies:

```$ npm install```

Start the development server:

```$ npm start```

And open [http://localhost:3000](http://localhost:3000) to view it in the browser.

## Packages added

`redux` This is a package to manage the app state making just one data source for the whole application.

`react-router` This package is used to navigate between diferent views of the app, separating layouts and improving organization.



